<?php
namespace Exam\Task\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Exam\Task\Api\Data\BlogPostInterface;
use Exam\Task\Api\Data\BlogPostSearchResultInterface;

interface BlogPostRepositoryInterface
{
    /**
     * @param int $id
     * @return BlogPostInterface
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param BlogPostInterface $develop
     * @return BlogPostInterface
     */
    public function save(BlogPostInterface $develop);

    /**
     * @param BlogPostInterface $develop
     * @return void
     */
    public function delete(BlogPostInterface $develop);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return BlogPostSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
