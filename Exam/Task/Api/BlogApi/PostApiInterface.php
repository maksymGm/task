<?php
namespace Exam\Task\Api\BlogApi;
/**
 * @api
 */
interface PostApiInterface
{
    /**
     * @api
     * @param int $postId
     * @return array
     */
    public function getPost($postId=null);
}
