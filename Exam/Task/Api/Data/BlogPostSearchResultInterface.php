<?php
namespace Exam\Task\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface BlogPostSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return BlogPostInterface[]
     */
    public function getItems();

    /**
     * @param BlogPostInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
