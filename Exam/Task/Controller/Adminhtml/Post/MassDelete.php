<?php
namespace Exam\Task\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Exam\Task\Model\ResourceModel\BlogPost\CollectionFactory;
use Exam\Task\Api\BlogPostRepositoryInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NotFoundException;
use Magento\Ui\Component\MassAction\Filter;

class MassDelete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level
     * @TODO: add to acl special and change here
     */
    const ADMIN_RESOURCE = 'Exam_Task::post_index';

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var BlogPostRepositoryInterface
     */
    private $postRepository;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param BlogPostRepositoryInterface $postRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        BlogPostRepositoryInterface $postRepository
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->postRepository = $postRepository;
        parent::__construct($context);
    }

    /**
     * Category delete action
     *
     * @return Redirect
     */
    public function execute(): Redirect
    {
        if (!$this->getRequest()->isPost()) {
            throw new NotFoundException(__('Page not found.'));
        }
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $postDeleted = 0;
        foreach ($collection->getItems() as $post) {
            //$this->postRepository->delete($post);
            $postDeleted++;
        }

        if ($postDeleted) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $postDeleted)
            );
        }
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('etadmin/post/index');
    }
}
