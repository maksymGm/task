<?php
namespace Exam\Task\Controller\Adminhtml\Post;

use Magento\Backend\App\Action\Context;
use Exam\Task\Model\BlogPostFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var BlogPostFactory
     */
    protected $postFactory;

    /**
     * Save constructor
     *
     * @param Context $context
     * @param BlogPostFactory $postFactory
     */
    public function __construct(
        Context $context,
        BlogPostFactory $postFactory
    )
    {
        $this->postFactory = $postFactory;
        parent::__construct($context);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $rowId = isset($data['id']) ? $data['id'] : '';
        if (!$data) {
            $this->_redirect('etadmin/post/index');
        }
        try {
            $rowData = $this->postFactory->create()->load($rowId);
            if (!$rowData->getId() && $rowId) {
                $this->messageManager->addErrorMessage(__('Row data no longer exist!'));
                $this->_redirect('etadmin/post/index');
            }
            $rowData->setData($data);
            $rowData->save();
            $this->messageManager->addSuccessMessage(__('Row data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        $this->_redirect('etadmin/post/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Exam_Task::post_edit');
    }
}
