<?php
namespace Exam\Task\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class View extends Action
{
    const ADMIN_RESOURCE = 'Exam_Task::post_view';

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Exam_Task::post_view');
        $resultPage->addBreadcrumb(__('Post'), __('View'));
        $resultPage->getConfig()->getTitle()->prepend(__('Post'));

        return $resultPage;
    }
}
