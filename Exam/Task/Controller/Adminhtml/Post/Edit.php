<?php
namespace Exam\Task\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Exam\Task\Model\BlogPostFactory;
use Magento\Framework\Registry;

class Edit extends Action implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var BlogPostFactory
     */
    protected $postFactory;
    /**
     * @var Registry;
     */
    private $coreRegistry;

    /**
     * @param Context $context
     * @param PageFactory $rawFactory
     * @param BlogPostFactory $postFactory
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        PageFactory $rawFactory,
        BlogPostFactory $postFactory,
        Registry $coreRegistry
    ) {
        $this->pageFactory = $rawFactory;
        $this->postFactory = $postFactory;
        $this->coreRegistry = $coreRegistry;

        parent::__construct($context);
    }

    /**
     * Add the main Admin Grid page
     *
     * @return Page
     */
    public function execute(): Page
    {
        $resultPage = $this->pageFactory->create();
        $rowId = (int) $this->getRequest()->getParam('id');
        $rowData = '';
        if ($rowId) {
            $rowData = $this->postFactory->create()->load($rowId);
            if (!$rowData || !$rowData->getId()) {
                $this->messageManager->addErrorMessage(__('Row data no longer exist!'));
                $this->_redirect('etadmin/post/index');
            }
        }
        $this->coreRegistry->register('row_data', $rowData);
        $title = $rowId ? __('Edit Post') : __('Add Post');
        $resultPage->setActiveMenu('Exam_Task::post_add');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Exam_Task::post_edit');
    }
}
