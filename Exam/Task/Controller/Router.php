<?php
namespace Exam\Task\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

class Router implements RouterInterface
{
    protected ActionFactory $actionFactory;

    protected ResponseInterface $response;

    /**
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    )
    {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|void
     */
    public function match(RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');

        if (strpos($identifier, 'etblogpost-i') !== false) {
            $request->setModuleName('cms')
                ->setControllerName('page')
                ->setActionName('view')
                ->setParam('page_id', 5);
        } else if (strpos($identifier, 'etblogpost') !== false) {
            $explode = explode('/', $identifier);
            $request->setModuleName('etblog')
                ->setControllerName('post')
                ->setActionName('view');

            $postId = 0;
            if (!empty($explode[1]) && is_numeric($explode[1])) {
                $postId = (int) $explode[1];
            }
            $request->setParams([
                'postId' => $postId
            ]);
        } else {
            return;
        }

        return $this
            ->actionFactory
            ->create('Magento\Framework\App\Action\Forward', ['request' => $request]);
    }
}
