<?php
namespace Exam\Task\Console\Command;

use Exam\Task\Api\Data\BlogPostInterfaceFactory;
use Exam\Task\Api\BlogPostRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PostCreateCommand extends Command
{
    const POST_TITLE = 'title';
    const POST_CONTENT = 'content';
    const POST_BLOG_ID = 'blog_id';

    private BlogPostRepositoryInterface $postRepository;
    private BlogPostInterfaceFactory $postFactory;

    /**
     * @param BlogPostRepositoryInterface $postRepository
     * @param BlogPostInterfaceFactory $postFactory
     * @param string|null $name
     */
    public function __construct(
        BlogPostRepositoryInterface $postRepository,
        BlogPostInterfaceFactory $postFactory,
        string $name = null
    ) {
        parent::__construct($name);
        $this->postRepository = $postRepository;
        $this->postFactory = $postFactory;
    }
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('et:post:create');
        $this->setDescription('Command to create post');
        $this->addOption(
            self::POST_TITLE,
            null,
            InputOption::VALUE_REQUIRED,
            'The title of the post'
        );
        $this->addOption(
            self::POST_CONTENT,
            null,
            InputOption::VALUE_OPTIONAL,
            'The content of the post'
        );
        $this->addOption(
            self::POST_BLOG_ID,
            null,
            InputOption::VALUE_OPTIONAL,
            'The blog ID of the post'
        );
        parent::configure();
    }

    /**
     * CLI command description
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        if (!$postTitle = $input->getOption(self::POST_TITLE)) {
            $output->writeln('<error>ERROR! EMPTY POST TITLE!</error>');
            return;
        }
        if (!$postContent = $input->getOption(self::POST_CONTENT)) {
            $postContent = 'Default post content. Random number is ' . rand(1000001, 9999999);
        }
        if (!$postBlogId = $input->getOption(self::POST_BLOG_ID)) {
            $postBlogId = 1;
        }
        $post = $this->postFactory->create(
            [
                'data' => [
                    'id' => NULL,
                    'blog_id' => $postBlogId,
                    'title' => $postTitle,
                    'content' => $postContent,
                    'created_at' => NULL

                ]
            ]
        );
        $post->setCreatedAt(date('Y-m-d H:i:s'));

        $output->writeln('<info>Post with id `' . $post->getId() . '`!</info>');
        $output->writeln('<info>Post with blog id `' . $post->getBlogId() . '`!</info>');
        $output->writeln('<info>Post with title `' . $post->getTitle() . '`!</info>');
        $output->writeln('<info>Post with content `' . $post->getContent() . '`!</info>');
        $output->writeln('<info>Post with date `' . $post->getCreatedAt() . '`!</info>');
        $res = $this->postRepository->save($post);
        if ($res) {
            $output->writeln('<info>Post with title `' . $res->getTitle() . '` is created!</info>');
        } else {
            $output->writeln('<error>Error! Database error!</error>');
        }
    }
}
