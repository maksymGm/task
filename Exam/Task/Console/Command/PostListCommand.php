<?php
namespace Exam\Task\Console\Command;

use Exam\Task\Api\BlogPostRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PostListCommand extends Command
{
    const START_POST_ID = 'start_post_id';
    private BlogPostRepositoryInterface $postRepository;
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @param BlogPostRepositoryInterface $postRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param string|null $name
     */
    public function __construct(
        BlogPostRepositoryInterface $postRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        string $name = null
    ) {
        parent::__construct($name);
        $this->postRepository = $postRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('et:post:list');
        $this->setDescription('Command for list the posts');
        $this->addOption(
            self::START_POST_ID,
            null,
            InputOption::VALUE_REQUIRED,
            'The id of the first post in list'
        );
        parent::configure();
    }

    /**
     * CLI command description
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        // TODO: Realize search starting post - after studing Database
        if (!$startPostId = (int) $input->getOption(self::START_POST_ID)) {
            $startPostId = 1;
        }
        $pageSize = 5;
        $this->searchCriteriaBuilder->setPageSize($pageSize);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $posts = $this->postRepository->getList($searchCriteria);
        $output->writeln('<info>The count of posts is `' . $posts->getTotalCount() . '`!</info>');
        foreach ($posts->getItems() as $post) {
            sleep(1);
            $output->writeln('<info>The post title is `' . $post->getTitle() . '`!</info>');
        }
    }
}
