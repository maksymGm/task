<?php
namespace Exam\Task\Console\Command;

use Exam\Task\Api\BlogPostRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PostDeleteCommand extends Command
{
    const POST_ID = 'post_id';
    private BlogPostRepositoryInterface $postRepository;

    public function __construct(
        BlogPostRepositoryInterface $postRepository,
        string $name = null
    ) {
        parent::__construct($name);
        $this->postRepository = $postRepository;
    }
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('et:post:delete');
        $this->setDescription('Command for delete post by id!');
        $this->addOption(
            self::POST_ID,
            null,
            InputOption::VALUE_REQUIRED,
            'The id of the post, which will be deleted!'
        );
        parent::configure();
    }

    /**
     * CLI command description
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        if (!$postId = (int) $input->getOption(self::POST_ID)) {
            $output->writeln('<error>ERROR! EMPTY POST ID!</error>');
            return;
        }
        $post = $this->postRepository->getById($postId);
        $this->postRepository->delete($post);

        $output->writeln('<info>Post with title `' . $post->getTitle() . '` was deleted!</info>');
    }
}
