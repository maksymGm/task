<?php
namespace Exam\Task\Plugin;

use Exam\Task\Ui\DataProvider\Post\ListingDataProvider as PostDataProvider;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;

class AddAttributesToUiDataProvider
{
    /** @var AttributeRepositoryInterface */
    private $attributeRepository;

    /** @var ProductMetadataInterface */
    private $productMetadata;

    /**
     * Constructor
     *
     * @param AttributeRepositoryInterface $attributeRepository
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(
        AttributeRepositoryInterface $attributeRepository,
        ProductMetadataInterface $productMetadata
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->productMetadata = $productMetadata;
    }

    /**
     * Get Search Result after plugin
     * @TODO: add blog_name after creating the table dm_blog
     *
     * @param PostDataProvider $subject
     * @param SearchResult $result
     * @return SearchResult
     */
    public function afterGetSearchResult(PostDataProvider $subject, SearchResult $result)
    {
        return $result;
        /*
        if ($result->isLoaded()) {
            return $result;
        }

        $column = 'blog_id';

        $attribute = $this->attributeRepository->get('dm_blog', 'blog_title');

        $result->getSelect()->joinLeft(
            ['etadminname' => $attribute->getBackendTable()],
            'etadminname.' . $column . ' = main_table.' . $column . ' AND etadminname.attribute_id = '
            . $attribute->getAttributeId(),
            ['name' => 'etadminname.value']
        );

        $result->getSelect()->where('etadminname.value LIKE "B%"');

        return $result;
        */
    }
}
