<?php
namespace Exam\Task\ViewModel;

use Exam\Task\Api\Data\BlogPostInterface;
use Exam\Task\Api\BlogPostRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class PostViewModel implements ArgumentInterface
{

    private BlogPostRepositoryInterface $postRepository;
    private RequestInterface $request;
    /**
     * @var null | BlogPostInterface
     */
    private $post;

    /**
     * @param BlogPostRepositoryInterface $postRepository
     * @param RequestInterface $request
     */
    public function __construct(
        BlogPostRepositoryInterface $postRepository,
        RequestInterface $request
    ) {
        $this->postRepository = $postRepository;
        $this->request = $request;
        $this->post = null;
    }

    /**
     * @param int $id
     * @return BlogPostInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getPost(int $id): BlogPostInterface
    {
        if (!($this->post && $this->post->getId() == $id)) {
            $this->post = $this->postRepository->getById($id);
        }
        return $this->post;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getTitle(): string
    {
        $postId = (int)$this->request->getParam('postId');
        if (empty($postId)) {
            return '';
        }
        if (!$post = $this->getPost($postId)) {
            return '';
        }
        return $post->getTitle();
    }

    public function getContent(): string
    {
        $postId = (int)$this->request->getParam('postId');
        if (empty($postId)) {
            return '';
        }
        if (!$post = $this->getPost($postId)) {
            return '';
        }
        return $post->getContent();
    }

    public function getCreatedAt(): string
    {
        $postId = (int)$this->request->getParam('postId');
        if (empty($postId)) {
            return '';
        }
        if (!$post = $this->getPost($postId)) {
            return '';
        }
        return $post->getCreatedAt();
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function hasPost(): bool
    {
        $postId = (int)$this->request->getParam('postId');
        if (empty($postId) || !$this->postRepository->hasPost($postId)) {
            return false;
        }
        return true;
    }

}
