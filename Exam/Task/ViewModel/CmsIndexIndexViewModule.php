<?php

namespace Exam\Task\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class CmsIndexIndexViewModule implements ArgumentInterface
{
    /**
     * @return string
     */
    public function getPageHeader(): string
    {
        return 'Luma Main Page!';
    }
}
