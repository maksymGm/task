<?php
namespace Exam\Task\Model;

use Magento\Framework\Api\SearchResults;
use Exam\Task\Api\Data\BlogPostSearchResultInterface;

class BlogPostSearchResult extends SearchResults implements BlogPostSearchResultInterface
{

}
