<?php
namespace Exam\Task\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class BlogPost extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('dm_blog_post', 'id');
    }
}
