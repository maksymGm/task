<?php
namespace Exam\Task\Model\ResourceModel\BlogPost;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Exam\Task\Model\ResourceModel\BlogPost as ResourceBlogPost;
use Exam\Task\Model\BlogPost;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(BlogPost::class, ResourceBlogPost::class);
    }
}
