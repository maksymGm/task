<?php
namespace Exam\Task\Model\BlogApi;

use Exam\Task\Api\BlogPostRepositoryInterface;
use Exam\Task\Api\BlogApi\PostApiInterface;
use Psr\Log\LoggerInterface;

/**
 * @api
 */
class PostApi implements PostApiInterface
{
    private BlogPostRepositoryInterface $postRepository;
    private LoggerInterface $logger;

    /**
     * @param BlogPostRepositoryInterface $postRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        BlogPostRepositoryInterface $postRepository,
        LoggerInterface $logger
    ) {
        $this->postRepository = $postRepository;
        $this->logger = $logger;
    }

    /**
     * @api
     * @param int $postId
     * @return array
     */
    public function getPost($postId=null)
    {
        $post = [];
        $message = '';
        $success = true;
        try {
            if (is_numeric($postId) && $postId > 0 && $this->postRepository->hasPost($postId)) {
                $postObj = $this->postRepository->getById($postId);
                if ($postObj && $postObj->getId()) {
                    $post = $this->postRepository->toArray($postObj);
                }
            } else {
                $success = false;
                $message = 'Undefined or wrong parameter post ID';
            }
        } catch (\Exception $e) {
            $post = [];
            $success = false;
            $message = $e->getMessage();
            $this->logger->warning($message);
        }
        $response = ['success' => $success, 'message' => $message, 'data' => $post];
        return $response;
    }
}
