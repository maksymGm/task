<?php
namespace Exam\Task\Model;

use Exam\Task\Api\BlogPostRepositoryInterface;
use Exam\Task\Api\Data\BlogPostInterface;
use Exam\Task\Model\ResourceModel\BlogPost as BlogPostResource;
use Exam\Task\Api\Data\BlogPostSearchResultInterfaceFactory;
use Exam\Task\Model\ResourceModel\BlogPost\CollectionFactory as BlogPostCollectionFactory;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class BlogPostRepository implements BlogPostRepositoryInterface
{
    /**
     * @var BlogPostFactory
     */
    private $blogPostFactory;

    /**
     * @var BlogPostResource
     */
    private $blogPostResource;

    /**
     * @var BlogPostCollectionFactory
     */
    private $blogPostCollectionFactory;

    /**
     * @var BlogPostSearchResultInterfaceFactory
     */
    private $blogPostSearchResultFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param BlogPostFactory $blogPostFactory
     * @param BlogPostResource $blogPostResource
     * @param BlogPostCollectionFactory $blogPostCollectionFactory
     * @param BlogPostSearchResultInterfaceFactory $blogPostSearchResultInterfaceFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        BlogPostFactory                      $blogPostFactory,
        BlogPostResource                     $blogPostResource,
        BlogPostCollectionFactory            $blogPostCollectionFactory,
        BlogPostSearchResultInterfaceFactory $blogPostSearchResultInterfaceFactory,
        CollectionProcessorInterface         $collectionProcessor
    ) {
        $this->blogPostFactory = $blogPostFactory;
        $this->blogPostResource = $blogPostResource;
        $this->blogPostCollectionFactory = $blogPostCollectionFactory;
        $this->blogPostSearchResultFactory = $blogPostSearchResultInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param int $id
     * @return BlogPostInterface|BlogPost
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $blogPost = $this->blogPostFactory->create();
        $this->blogPostResource->load($blogPost, $id, 'id');
        //$blogPost->getResource()->load($blogPost, $id);
        if (! $blogPost->getId()) {
            throw new NoSuchEntityException(__('Unable to find post with ID "%1"', $id));
        }
        return $blogPost;
    }

    /**
     * @param BlogPostInterface $blogPost
     * @return BlogPostInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(BlogPostInterface $blogPost)
    {
        $this->blogPostResource->save($blogPost);
        return $blogPost;
    }

    /**
     * @param BlogPostInterface $blogPost
     * @throws \Exception
     */
    public function delete(BlogPostInterface $blogPost)
    {
        $this->blogPostResource->delete($blogPost);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $collection = $this->blogPostCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->blogPostSearchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return array
     */
    public function getItems(SearchCriteriaInterface $searchCriteria): array
    {
        return $this->getList($searchCriteria)->getItems();
    }

    /**
     * @param $id
     * @return bool
     */
    public function hasPost($id)
    {
        $blogPost = $this->blogPostFactory->create();
        $this->blogPostResource->load($blogPost, $id, 'id');
        //$blogPost->getResource()->load($blogPost, $id);
        if (!$blogPost->getId()) {
            return false;
        }
        return true;
    }

    /**
     * @param BlogPostInterface $blogPost
     * @return array
     */
    public function toArray(BlogPostInterface $blogPost): array
    {
        $post = [
            'id' => $blogPost->getId(),
            'blog_id' => $blogPost->getBlogId(),
            'title' => $blogPost->getTitle(),
            'content' => $blogPost->getContent(),
            'created_at' => $blogPost->getCreatedAt(),
        ];
        return $post;
    }
}
