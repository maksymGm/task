<?php
namespace Exam\Task\Model;

use Magento\Framework\Model\AbstractModel;
use Exam\Task\Model\ResourceModel\BlogPost as ResourceBlogPost;
use Exam\Task\Api\Data\BlogPostInterface;

class BlogPost extends AbstractModel implements BlogPostInterface
{
    const ID = 'id';
    const BLOG_ID = 'blog_id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const CREATED_AT = 'created_at';

    protected function _construct()
    {
        $this->_init(ResourceBlogPost::class);
    }

    /**
     * @return int
     */
    public function getBlogId(): int
    {
        return (int) $this->_getData(self::BLOG_ID);
    }

    /**
     * @param int $blogId
     * @return BlogPost
     */
    public function setBlogId(int $blogId)
    {
        return $this->setData(self::BLOG_ID, $blogId);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return (string) $this->_getData(self::TITLE);
    }

    /**
     * @param string $title
     * @return BlogPost
     */
    public function setTitle(string $title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return (string) $this->_getData(self::CONTENT);
    }

    /**
     * @param string $content
     * @return BlogPost
     */
    public function setContent(string $content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string) $this->_getData(self::CREATED_AT);
    }

    /**
     * @param string $createdAt
     * @return BlogPost
     */
    public function setCreatedAt(string $createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
