<?php
namespace Exam\GraphQl\Sandbox;

use Exam\Task\Api\BlogPostRepositoryInterface;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class GraphQl implements ArgumentInterface
{
    private BlogPostRepositoryInterface $postRepository;

    public function __construct(
        BlogPostRepositoryInterface $postRepository,
        SearchCriteriaInterface $searchCriteria,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->postRepository = $postRepository;
        $this->searchCriteria = $searchCriteria;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param array $args
     * @return array
     * @throws \Magento\Framework\Exception\InputException
     */
    public function testGraphQl(array $args = []): array
    {
        $blogId = null;
        if (!empty($args['blog_id'])) {
            $blogId = $args['blog_id'];
        }
        $sort = '';
        if (!empty($args['sort'])) {
            $sort = $args['sort'];
        }
        if (!empty($blogId)) {
            $filter1 = new Filter();
            $filter1->setField('blog_id')
                ->setValue($blogId)
                ->setConditionType('eq');
            $filterGroup = new FilterGroup();
            $filterGroup->setFilters([$filter1]);
            $this->searchCriteria->setFilterGroups([$filterGroup]);
        }
        if ($sort && in_array($sort, ['ASC', 'DESC'])) {
            $sortOrder = new SortOrder();
            $sortOrder
                ->setField("title")
                ->setDirection($sort);
            $this->searchCriteria->setSortOrders([$sortOrder]);
        }

        $posts = $this->postRepository->getList($this->searchCriteria);
        $postList = [];
        if (!$posts || !$posts->getTotalCount()) {
            return $postList;
        }
        foreach ($posts->getItems() as $post) {
            $postList[] = [
                'id' => '' . $post->getId(),
                'title' => '' . $post->getTitle(),
                'content' => '' . $post->getContent(),
                'created_at' => '' . $post->getCreatedAt()
            ];
        }
        return $postList;
    }
}
