<?php
namespace Exam\GraphQl\GraphQl\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Exam\Task\Api\BlogPostRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;

class GetPostList implements ResolverInterface
{
    private BlogPostRepositoryInterface $postRepository;
    private SearchCriteriaInterface $searchCriteria;

    /**
     * @param BlogPostRepositoryInterface $postRepository
     * @param SearchCriteriaInterface $searchCriteria
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        BlogPostRepositoryInterface $postRepository,
        SearchCriteriaInterface $searchCriteria
    ) {
        $this->postRepository = $postRepository;
        $this->searchCriteria = $searchCriteria;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array
     * @throws \Magento\Framework\Exception\InputException
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $blogId = null;
        if (!empty($args['blog_id'])) {
            $blogId = $args['blog_id'];
        }
        $sort = '';
        if (!empty($args['sort'])) {
            $sort = $args['sort'];
        }
        if (!empty($blogId)) {
            $filter1 = new Filter();
            $filter1->setField('blog_id')
                ->setValue($blogId)
                ->setConditionType('eq');
            $filterGroup = new FilterGroup();
            $filterGroup->setFilters([$filter1]);
            $this->searchCriteria->setFilterGroups([$filterGroup]);
        }
        if ($sort && in_array($sort, ['ASC', 'DESC'])) {
            $sortOrder = new SortOrder();
            $sortOrder
                ->setField("title")
                ->setDirection($sort);
            $this->searchCriteria->setSortOrders([$sortOrder]);
        }

        $posts = $this->postRepository->getList($this->searchCriteria);
        $postList = [];
        if (!$posts || !$posts->getTotalCount()) {
            return $postList;
        }
        foreach ($posts->getItems() as $post) {
            $postList[] = [
                'id' => '' . $post->getId(),
                'title' => '' . $post->getTitle(),
                'content' => '' . $post->getContent(),
                'created_at' => '' . $post->getCreatedAt()
            ];
        }
        return $postList;
    }
}
