<?php
namespace Exam\GraphQl\GraphQl\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Exam\Task\Api\BlogPostRepositoryInterface;

class GetPost implements ResolverInterface
{

    private BlogPostRepositoryInterface $postRepository;

    /**
     * @param BlogPostRepositoryInterface $postRepository
     */
    public function __construct(
        BlogPostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return string[]
     * @throws GraphQlInputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (empty($id = $args['id'])) {
            throw new GraphQlInputException(__('ID is required!'));
        }
        $post = $this->postRepository->getById($id);
        return [
            'id' => '' . $post->getId(),
            'title' => '' . $post->getTitle(),
            'content' => '' . $post->getContent(),
            'created_at' => '' . $post->getCreatedAt()
        ];
    }
}
